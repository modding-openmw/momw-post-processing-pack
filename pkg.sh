#!/bin/sh
set -eu

file_name=momw-post-processing-pack.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

mv web/site/img/momw-post-processing-pack*.png .

zip --must-match --recurse-paths $file_name \
    00\ RecommendedConfig \
    01\ XE-Shaders \
    02\ OMWFX-Shaders \
    03\ ZesterersVolumetricClouds \
    04\ WareyaOpenMWShaders \
    05\ EpochPostProcessShaders \
    06\ EpochReshadePorts \
    07\ ZesterersSSAO \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt \
    momw-post-processing-pack*.png \
    --exclude \*.git

mv momw-post-processing-pack*.png web/site/img/

sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt
