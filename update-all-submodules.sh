#!/bin/bash
set -euo pipefail

submodules=(
    "01 XE-Shaders"
    "02 OMWFX-Shaders"
    "03 ZesterersVolumetricClouds"
    "04 WareyaOpenMWShaders"
    "05 EpochPostProcessShaders"
    "06 EpochReshadePorts"
    "07 ZesterersSSAO"
)

this_dir=$(realpath "$(dirname "${0}")")
cd "$this_dir"

for sm in "${submodules[@]}"; do
    cd "$sm"
    git pull
    cd -
done
