# MOMW Post Processing Pack

A collection of post processing shaders for OpenMW.

Includes:

* [XE-Shaders by akortunov and various others](https://gitlab.com/akortunov/xe-shaders)
* [OMWFX-Shaders by vtastek and various others](https://gitlab.com/vtastek/omwfx-shaders)
* [OpenMW Volumetric Clouds by Zesterer](https://github.com/zesterer/openmw-volumetric-clouds)
* [OpenMW-Shaders by wareya](https://github.com/wareya/OpenMW-Shaders)
* [Post Process Shaders by Epoch](https://github.com/EpochWon/OpenMW-PostProcessShaders)
* [ReShade Ports by Epoch](https://github.com/EpochWon/OpenMW-ReshadePorts)
* [High Quality, High Performance Screen-Space Ambient Occlusion by Zesterer](https://github.com/zesterer/openmw-ssao)

**Requires OpenMW 0.48 or newer!**

#### Credits

**Packaging**: johnnyhostile

**Shader authors**:

* Andrei Kortunov for [`01 XE-Shaders`](https://gitlab.com/akortunov/xe-shaders)
* Dexter, wazabear, Andrei Kortunov, and others for [`02 OMWFX-Shaders`](https://gitlab.com/vtastek/omwfx-shaders)
* Zesterer for [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds) and [`07 ZesterersSSAO`](https://github.com/zesterer/openmw-ssao)
* Wareya for [`04 WareyaOpenMWShaders`](https://github.com/wareya/OpenMW-Shaders)
* Epoch for [`05 EpochPostProcessShaders`](https://github.com/EpochWon/OpenMW-PostProcessShaders) and [`06 EpochReshadePorts`](https://github.com/EpochWon/OpenMW-ReshadePorts)

**Special Thanks**:

* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/momw-post-processing-pack/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/momw-post-processing-pack)

#### Installation

1. Download the zip from a link above.
1. Extract the contents to a location of your choosing, examples below:

```
# Windows
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\01 XE-Shaders
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\02 OMWFX-Shaders
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\03 ZesterersVolumetricClouds
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\04 WareyaOpenMWShaders
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\05 EpochPostProcessShaders
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\06 EpochReshadePorts
C:\games\OpenMWMods\Shaders\momw-post-processing-pack\07 ZesterersSSAO

# Linux
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/01 XE-Shaders
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/02 OMWFX-Shaders
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/03 ZesterersVolumetricClouds
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/04 WareyaOpenMWShaders
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/05 EpochPostProcessShaders
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/06 EpochReshadePorts
/home/username/games/OpenMWMods/Shaders/momw-post-processing-pack/07 ZesterersSSAO

# macOS
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/01 XE-Shaders
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/02 OMWFX-Shaders
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/03 ZesterersVolumetricClouds
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/04 WareyaOpenMWShaders
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/05 EpochPostProcessShaders
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/06 EpochReshadePorts
/Users/username/games/OpenMWMods/Shaders/momw-post-processing-pack/07 ZesterersSSAO
```

1. Add the appropriate data paths to your `openmw.cfg` file, for example:

```
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\01 XE-Shaders"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\02 OMWFX-Shaders"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\03 ZesterersVolumetricClouds"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\04 WareyaOpenMWShaders"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\05 EpochPostProcessShaders"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\06 EpochReshadePorts"
data="C:\games\OpenMWMods\Shaders\momw-post-processing-pack\07 ZesterersSSAO"
```

1. Enable shaders as desired using F2 while in game.
1. Optionally use the recommended load order (this goes into `settings.cfg` under the `[Post Processing]` section):

        chain = reshade-CAS,ssao_hq,underwater_interior_effects,underwater_effects,clouds,godrays,bloomlinear,hdr,FollowerAA,depth_of_field,reshade-colourfulness

1. Also optional: use the preconfigured `shaders.yaml` included in `00 RecommendedConfig` (put it next to your `settings.cfg` and `openmw.cfg` files).

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
<!-- * Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/#TODO?tab=posts) -->
