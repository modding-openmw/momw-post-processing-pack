## MOMW Post Processing Pack

#### 1.11

* Updated [Post Process Shaders by Epoch](https://github.com/EpochWon/OpenMW-PostProcessShaders) to `466a3bf938cd5330daf4f9a5b9d88fd278a1e99f`
* Updated [ReShade Ports by Epoch](https://github.com/EpochWon/OpenMW-ReshadePorts) to `0f91bc2b026d8c20304cfc40bf536ccaf07fbbcb`

<!-- [Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/#TODO) -->

#### 1.10

* Updated [`01 XE-Shaders`](https://gitlab.com/akortunov/xe-shaders) from `e3d2441a2b39bc699228c665afdb53e3c19f762e` to `7e273a050bb30124fb69e55bf451d0711bca8696`
* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds) from `a8627a82c46b2fea3008f278b18860cfa5e6c0f3` to `f06c94e7f48500a27e79b1144acffe3fae617127`
* Updated [`04 WareyaOpenMWShaders`](https://github.com/wareya/OpenMW-Shaders) from `65987e9350308cf1edfeaa013ddb6254d498813d` to `dd5800e7cf7b01783d3299ee5b7713987e50f1d1`
* Added [Post Process Shaders by Epoch](https://github.com/EpochWon/OpenMW-PostProcessShaders)
* Added [ReShade Ports by Epoch](https://github.com/EpochWon/OpenMW-ReshadePorts)
* Added [High Quality, High Performance Screen-Space Ambient Occlusion by Zesterer](https://github.com/zesterer/openmw-ssao)
* Updated the recommended config for the new and updated shaders listed above

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/34233414)

#### 1.9

* Set the recommended settings value for the `blur_fog` value in the `depth_of_field` shader to false
* Set the recommended settings value for the `bluenoise` value in the `godrays` shader to false

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/33481441)

#### 1.8

* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds) to `f06c94e7f48500a27e79b1144acffe3fae617127`
* Updated [`04 WareyaOpenMWShaders`](https://github.com/wareya/OpenMW-Shaders) to `dd5800e7cf7b01783d3299ee5b7713987e50f1d1`

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/29200203)

#### 1.7

* Updated [`01 XE-Shaders`](https://gitlab.com/akortunov/xe-shaders) to `e3d2441a2b39bc699228c665afdb53e3c19f762e`

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/22879985)

#### 1.6

* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds) to `a8627a82c46b2fea3008f278b18860cfa5e6c0f3`

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/22161895)

#### 1.5

* Updated [`02 OMWFX-Shaders`](https://gitlab.com/vtastek/omwfx-shaders) to `5bda0ce`

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/21213253)

#### 1.4

* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds) to `641919dab3674e15f3ed328f5c97a3dcc1f2ac6f`, updated some values in the recommended config

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/20240802)

#### 1.3

* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds)

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/20118707)

#### 1.2

* Updated [`03 ZesterersVolumetricClouds`](https://github.com/zesterer/openmw-volumetric-clouds), interior mist now works and is no longer disabled by the recommended config.

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/20019832)

#### 1.1

* Updated documentation to more explicitly describe what's included and where it comes from.

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/19598886)

#### 1.0

* Initial release

[Download Link](https://gitlab.com/modding-openmw/momw-post-processing-pack/-/packages/17158696)
